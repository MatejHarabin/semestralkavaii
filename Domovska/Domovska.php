<?php require_once "../Logika/header.php";
require_once "../Databaza/DBStorage.php";
$storage = new DBStorage();
?>

<br><br><br>
<div class="container">

    <?php if (isset($_GET['success'])) { ?>

        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <?php echo $_GET['success']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>

    <?php if (isset($_GET['error'])) { ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?php echo $_GET['error']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>

    <?php foreach ($storage->fetchArticles() as $article) { ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="card border-info mb-3" style="width: 70rem; ">
                    <div class="card-body">
                        <h5 class="card-title"> <?= $article->getHeading() ?> </h5>
                        <p class="card-text"> <?= $article->getText() ?> </p>

                    </div>
                </div>
            </div>
        </div>
        <p> Komentáre </p>
        <?php foreach ($storage->fetchCommentsByArticleId($article->getId()) as $comment) {?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card border-danger mb-3" style="width: 50rem;">
                        <div class="card-body">
                            <p class="card-text"> <?php echo $comment->getText() ?>   </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <br><br>


    <?php } ?>
</div>

<?php include "../Logika/footer.php"; ?>
