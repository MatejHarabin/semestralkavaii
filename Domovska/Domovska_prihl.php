<?php
require_once "../Databaza/DBStorage.php";
$storage = new DBStorage();

?>


<?php   require_once "../Logika/header_prihl.php"?>

<br><br><br>



    <?php

    $username = $_SESSION['username'];
    $password = $_SESSION['password'];
    $user_id = $storage->getUserIdFromUsers($username, $password);

    foreach ($storage->fetchArticles() as $article) {
        $userIdOfArticle = $storage->fetchUserIdOfArticle($article->getId());
        if($user_id == $userIdOfArticle) {?>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="card border-info mb-3" style="width: 70rem;">
                <div class="card-body">
                    <h5 class="card-title"> <?= $article->getHeading() ?> </h5>
                    <p class="card-text"> <?= $article->getText() ?> </p>
                    <div class="d-flex justify-content-end">

                        <form method="post" action="../Logika/Forms/edit_form.php">
                            <input type="submit" class="btn btn-warning me-2" name="idEdit" value="Upraviť"/>
                            <input type="hidden" name="idEdit" value="<?= $article->getId(); ?>"/>
                        </form>

                        <form method="post" action="../Logika/Article/deleteArticle.php">
                            <input type="submit" class="btn btn-danger me-2" value="Vymazať"/>
                            <input type="hidden" name="idDelete" value="<?= $article->getId(); ?>"/>
                        </form>

                        <form method="post" action="../Logika/Forms/comment_form.php">
                            <input type="submit" class="btn btn-primary" value="Pridať komentár"/>
                            <input type="hidden" name="idComment" value="<?= $article->getId(); ?>"/>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <p> Komentáre </p>
            <?php foreach ($storage->fetchCommentsByArticleId($article->getId()) as $comment) {?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card border-danger mb-3" style="width: 50rem;">
                        <div class="card-body">
                            <p class="card-text"> <?php echo $comment->getText() ?>   </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <br><br>
</div>
        <?php } else { ?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card border-info mb-3" style="width: 70rem;">
                            <div class="card-body">
                                <h5 class="card-title"> <?= $article->getHeading() ?> </h5>
                                <p class="card-text"> <?= $article->getText() ?> </p>
                                <div class="d-flex justify-content-end">

                                     <form method="post" action="../Logika/Forms/comment_form.php">
                                        <input type="submit" class="btn btn-primary" value="Pridať komentár"/>
                                        <input type="hidden" name="idComment" value="<?= $article->getId(); ?>"/>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p> Komentáre </p>
                <?php foreach ($storage->fetchCommentsByArticleId($article->getId()) as $comment) {?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card border-danger mb-3" style="width: 50rem;">
                                <div class="card-body">
                                    <p class="card-text"> <?php echo $comment->getText() ?>   </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <br><br>
            </div>


        <?php } ?>
<?php } ?>


<?php require_once "../Logika/footer.php"; ?>