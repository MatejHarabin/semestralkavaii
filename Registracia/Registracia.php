<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matej Harabin - tréner stolného tenisu</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="Registracia.css" >
</head>
<body>

    <?php if (isset($_GET['error'])) { ?>

    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
        <?php echo $_GET['error']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php } ?>

    <form id="register_form" action="../Logika/User/register.php" method="post">
        <div class="wrapper">
            <div class="form-area">
                <div class="form-text">
                    <h1> Zaregistrovať sa</h1>
                    <div class="Meno">
                        <label class="form-label">Uživateľ. meno</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="username" required>
                    </div>

                    <div class="Heslo">
                        <label class="form-label">Heslo</label>
                        <i class="fa fa-unlock-alt"></i>
                        <input type="password" name="password" required>
                    </div>

                    <div class="Krstné meno">
                        <label class="form-label">Meno</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="firstname" required>
                    </div>

                    <div class="Priezvisko">
                        <label class="form-label">Priezvisko</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="surname" required>
                    </div>

                    <div class="Vek">
                        <label class="form-label">Vek</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="age" required>
                    </div>

                    <div class="Váha">
                        <label class="form-label">Váha</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="weight" required>
                    </div>

                    <div class="Výška">
                        <label class="form-label">Výška</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="height" required>
                    </div>

                    <div class="Dominantná ruka">
                        <label class="form-label">Dominantná ruka</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="handedness" required>
                    </div>

                    <div class="Štýl hry">
                        <label class="form-label">Štýl hry</label>
                        <i class="fa fa-users"></i>
                        <input type="text" name="playmode" required>
                    </div>

                    <div class="Farba">
                        <label class="form-label">Farba používateľa</label>
                        <i class="fa fa-users"></i>
                        <input type="color" class="form-control form-control-color" id="exampleColorInput" value="#563d7c" title="Vyber si farbu" name="color">
                    </div>

                    <button name="register">Zaregistrovať</button>
                    <a href="../Domovska/Domovska.php" id="Domov">Domov</a>
                    <a href="../Prihlasovanie/Prihlasovanie.php" id="Prihlasit">Prihlásiť sa</a>
                </div>
            </div>
        </div>
    </form>
</body>
</html>