<?php
require_once "../Databaza/DBStorage.php";
$storage = new DBStorage();

?>

 <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Matej Harabin - tréner stolného tenisu</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>


        <link href='https://css.gg/user.css' rel='stylesheet'>
        <link rel="stylesheet" href="../Dizajn.css" >


    </head>
<body>
<header>

    <?php if (isset($_GET['success'])) { ?>

        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?php echo $_GET['success']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>

    <?php if (isset($_GET['error'])) { ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?php echo $_GET['error']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>



    <nav class="navbar navbar-expand-lg navbar-light bg-danger">
        <div class="container-fluid">

            <i class="gg-user"></i>
            <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle ms-3 me-5" data-bs-toggle="dropdown" aria-expanded="false">
                    <?php
                    session_start();
                    $username = $_SESSION['username'];
                    $password = $_SESSION['password'];

                        $nameOfUser = $storage->getUser($username, $password);
                        $i = 0;
                        $pom = null;
                        $pom2 = null;
                        foreach($nameOfUser as $value){
                            $pom = $value;
                            $pom2 = $pom2 . " " . $pom;
                            $i++;
                        }
                        echo $pom2;







                    ?>
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="Forms/create_form.php">Vytvor nový článok</a></li>
                    <li><a class="dropdown-item" href="../Plan/index.html">Rezervuj tréning</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="../Domovska/Domovska.php">Odhlásiť sa</a></li>
                </ul>
            </div>

            <a class="navbar-brand" href="#"><img src="../Domovska/logo.png" id="logo" alt="Späť na počiatočnú"></a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mb-2 mb-lg-0 justify-content-center" id="navigacia">

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="../Domovska/Domovska_prihl.php">Domov</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="../Plan/Plan_prihl.php">Plán</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Oznamy</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Výsledky</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Štatistiky</a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
</header>