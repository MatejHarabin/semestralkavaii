<?php
require_once "../../Databaza/DBStorage.php";


$storage = new DBStorage();

session_start();
$username = $_SESSION['username'];
$password = $_SESSION['password'];

date_default_timezone_set('Europe/Budapest');
$publish_date = null;

if (isset($_POST['create'])) {
    $heading = $_POST['heading'];
    $text = $_POST['text'];
    $publish_date = date_create()->format('Y-m-d H:i:s');

    $edit_date = null;
    $user_id = $storage->getUserIdFromUsers($username, $password);


}

$clanok = new Article($heading, $text, $publish_date, $edit_date, $user_id);


if (empty($heading)) {
    header('Location: ../Forms/create_form.php?error=Musíte vložiť nadpis&$heading');
} else if (empty($text)) {
        header('Location: ../Forms/create_form.php?error=Musíte vložiť text&$text');
        } else if ($storage->addArticle($clanok)){

            header('Location: ../../Domovska/Domovska_prihl.php?success=Článok úspešne vložený');
            } else {
                header('Location: ../../Domovska/Domovska_prihl.php?success=Takýto článok už existuje');
            }





