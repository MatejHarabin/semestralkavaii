<?php
require_once "../../Databaza/DBStorage.php";


$storage = new DBStorage();

/*if (isset($_POST['edit'])) {
    $heading = $_POST['heading'];
    $text = $_POST['text'];

}*/



$heading = $_POST['heading'];
$text = $_POST['text'];
$id = $_POST['id'];
$edit_date = date_create()->format('Y-m-d H:i:s');




if (empty($heading)) {
    header('Location: ../Forms/edit_form.php?error=Musíte vložiť nadpis&$heading');
    } else if (empty($text)) {
        header('Location: ../Forms/edit_form.php?error=Musíte vložiť text&$text');
    } else {
        $storage->editArticle($heading, $text, $id, $edit_date);
        header('Location: ../../Domovska/Domovska_prihl.php?success=Článok úspešne upravený');
    }
