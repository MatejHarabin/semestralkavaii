<br><br><br><br><br><br><br>

<footer>
    <div class="footer mt-2">
        <div class="page-footer font-small pt-4">
            <div class="container-fluid text-center text-md-left">
                <div class="row">
                    <div class="col-md-12 mt-md-0 mt-3">
                        <h5 class="text-uppercase" style="color: gainsboro">Kontakt</h5>
                        <p id="textKontakt">Nižná brána, s.č. 2250, 060 01 Kežmarok<br>
                            GPS súradnice: N 49.1424394 E 20.429997100000037<br>
                            Osobne:<br>
                            Pondelok: 19.30 – 21.00<br>
                            Streda: 19.30 – 21.00<br>
                            Piatok: 19.30 – 21.00<br>
                            Mestská športová hala Vlada Jančeka, herňa – bývalé kino,<br>
                            Matej Harabin<br>
                            Tel. číslo: 0907 898 385<br>
                            E-mail: ppcfortuna@centrum.sk
                        </p>
                    </div>
                </div>
            </div>

            <div class="footer-copyright text-center py-3" style="color: gainsboro">© 2020 Copyright:
                <a href="https://mdbootstrap.com/education/bootstrap/" style="color:Tomato;" target="_blank">
                    MDBootstrap.com</a>
            </div>
        </div>
    </div>

</footer>
</body>
</html>
