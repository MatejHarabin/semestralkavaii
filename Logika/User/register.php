<?php
require_once "../../Databaza/DBStorage.php";


$storage = new DBStorage();

if (isset($_POST['register'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = 'user';
    $firstname = $_POST['firstname'];
    $surname = $_POST['surname'];
    $age = $_POST['age'];
    $weight = $_POST['weight'];
    $height = $_POST['height'];
    $handedness = $_POST['handedness'];
    $playmode = $_POST['playmode'];
    $color = $_POST['color'];

}

$edit_color = hexdec($color);
if($username == 'admin') {
    $role = 'admin';
}

$user = new User($username, $password, $role,
                 $firstname, $surname, $age,
                 $weight, $height, $handedness,
                 $playmode, $edit_color);

if ($storage->addUser($user)){
    header('Location: ../../Prihlasovanie/Prihlasovanie.php?success=Registrácia prebehla úspešne');
} else {
    header('Location: ../../Registracia/Registracia.php?error=Používateľ s takýmto menom  už existuje');
}