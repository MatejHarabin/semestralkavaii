<?php
require_once "../../Databaza/DBStorage.php";


$storage = new DBStorage();

session_start();
$username = $_POST['username'];
$password = $_POST['password'];

if ($storage->signUser($username, $password)) {
    $_SESSION['username'] = $username;
    $_SESSION['password'] = $password;
    header('Location: ../header_prihl.php?success=Prihlásenie bolo úspešné');
} else {
    header('Location: ../../Prihlasovanie/Prihlasovanie.php?error=Nesprávne meno alebo heslo');
}