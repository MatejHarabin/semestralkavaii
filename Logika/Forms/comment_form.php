<?php require_once "../header_prihl.php";
require_once "../../Databaza/DBStorage.php";
$storage = new DBStorage();
?>

<br><br><br><br><br>
    <div class="container">
        <?php
        $id = $_POST['idComment'];
        foreach ($storage->fetchArticlesById($id) as $article) { ?>
        <form id="article_form" action="../Comment/commentary.php" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card border-info mb-3" style="width: 70rem;">
                        <div class="card-body">
                            <h5 class="card-title"> <?= $article->getHeading() ?> </h5>
                            <p class="card-text"> <?= $article->getText() ?> </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                      <div class="card-body">
                            <div class="mb-5" id="article_form_text">
                                <input type="text" class="form-control" id="exampleFormControlInput1" name="text"
                                       placeholder="Vlož komentár" style="width: 60rem" required>
                            </div>
                        </div>
                   </div>
            </div>

            <input type="hidden" name="id" value="<?= $article->getId(); ?>"/>
            <button type="submit" class="btn btn-primary" id="tlacidloKomentar" name="edit">Pridať komentár</button>
        </form>
    </div>
    <?php } ?>

<?php require_once "footer.php"; ?>


