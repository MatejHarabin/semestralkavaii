<?php require_once "../header_prihl.php";
require_once "../../Databaza/DBStorage.php";
$storage = new DBStorage();
?>

<br>
<div class="container">
    <?php
    $id = $_POST['idEdit'];
    foreach ($storage->fetchArticlesById($id) as $article) { ?>
    <form id="article_form" action="../Article/editArticle.php" method="post">
        <h3 class="display-4 text-center mb-4"> Upraviť článok</h3>

        <?php if (isset($_GET['error'])) { ?>

            <div class="alert alert-danger" role="alert" id="alert">
                <?php echo $_GET['error']; ?>
            </div>

        <?php } ?>


        <div class="mb-3 mt-3">
            <label for="exampleFormControlInput1" class="form-label">Nadpis článku</label>
            <input type="text" value="<?= $article->getHeading() ?>" class="form-control" id="exampleFormControlInput1" name="heading">
        </div>
        <div class="mb-5" id="article_form_text">
            <label for="exampleFormControlTextarea1" class="form-label">Text článku</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="text"
                      placeholder="Vlož text"><?= $article->getText() ?></textarea>
        </div>

        <input type="hidden" name="id" value="<?= $article->getId(); ?>"/>

        <button type="submit" class="btn btn-warning" id="tlacidlo" name="edit">Upraviť</button>
    </form>
    <?php } ?>
</div>

<br>
<?php require_once "footer.php"; ?>
