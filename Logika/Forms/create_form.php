<?php require_once "../header_prihl.php"; ?>
<br>
<div class="container">
    <form id="article_form" action="../Article/createArticle.php" method="post">
        <h3 class="display-4 text-center mb-4"> Vytvoriť článok</h3>

        <?php if (isset($_GET['error'])) { ?>

        <div class="alert alert-danger" role="alert" id="alert">
            <?php echo $_GET['error']; ?>
        </div>

        <?php } ?>
        <div class="mb-3 mt-3">
            <label for="exampleFormControlInput1" class="form-label">Nadpis článku</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="heading"
                   placeholder="Vlož nadpis" required>
        </div>
        <div class="mb-5" id="article_form_text">
            <label for="exampleFormControlTextarea1" class="form-label">Text článku</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="text"
                      placeholder="Vlož text" required></textarea>
        </div>

        <button type="submit" class="btn btn-primary" id="tlacidlo" name="create">Vytvoriť</button>
    </form>
</div>

<br>
<?php require_once "footer.php"; ?>
