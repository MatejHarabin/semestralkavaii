<?php
require_once "../../Databaza/DBStorage.php";


$storage = new DBStorage();

$id_article = $_POST['id'];
$publish_date = date_create()->format('Y-m-d H:i:s');
$text = $_POST['text'];


$komentar = new Comment($id_article, $publish_date, $text);

if (empty($text)) {
    header('Location: ../Forms/comment_form.php?error=Musíte vložiť text&$text');
} else if ($storage->addComment($komentar)){
    header('Location: ../../Domovska/Domovska_prihl.php?success=Komentár úspešne vložený');
} else {
    header('Location: ../../Domovska/Domovska_prihl.php?success=Takýto článok už existuje');
}
