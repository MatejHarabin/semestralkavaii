<?php

class Plan
{
    private $id_plan;
    private $id_user;
    private $training_date;
    private $location;


    /**
     * @return mixed
     */
    public function getIdPlan()
    {
        return $this->id_plan;
    }

    /**
     * @param mixed $id_plan
     */
    public function setIdPlan($id_plan): void
    {
        $this->id_plan = $id_plan;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user): void
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getTrainingDate()
    {
        return $this->training_date;
    }

    /**
     * @param mixed $training_date
     */
    public function setTrainingDate($training_date): void
    {
        $this->training_date = $training_date;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */

}