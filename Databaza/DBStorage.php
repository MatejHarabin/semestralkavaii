<?php
require_once "Comment.php";
require_once "Article.php";
require_once "User.php";


//úprava databázy
class DBStorage
{
    private const DB_HOST = 'localhost';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';
    private const DB_NAME = 'database';

    private $databaza;

    public function __construct()
    {
        try {
            $this->databaza = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
            $this->databaza->setAttribute(PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    function checkArticle(Article $clanok): bool
    {
        $sql = 'SELECT id FROM articles WHERE heading=? AND text=?';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([$clanok->getHeading(), $clanok->getText()]);
        if ($result = $stmt->fetch()) {
            return false;
        } else {
            return true;
        }
    }


    function addArticle(Article $clanok): bool
    {
        if ($this->checkArticle($clanok)) {
            $stmt = $this->databaza->prepare('INSERT INTO articles (heading, text, publish_date, edit_date, user_id) VALUES(?,?,?,?,?)');
            $stmt->execute([$clanok->getHeading(), $clanok->getText(), $clanok->getPublishDate(), $clanok->getEditDate(), $clanok->getUserId()]);
            return true;
        } else {
            return false;
        }
    }

    function addComment(Comment $komentar): bool
    {
        if ($this->checkComment($komentar)) {
            $stmt = $this->databaza->prepare('INSERT INTO comments (id_article, publish_date, text) VALUES(?,?,?)');
            $stmt->execute([$komentar->getIdArticle(), $komentar->getPublishDate(), $komentar->getText()]);
            return true;
        } else {
            return false;
        }
    }

    function checkComment(Comment $komentar): bool
    {
        $sql = 'SELECT id_comment FROM comments WHERE text=?';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([$komentar->getText()]);
        if ($result = $stmt->fetch()) {
            return false;
        } else {
            return true;
        }
    }
    /**
     *
     * @return Article[]
     */
    function fetchArticles()
    {
        $stmt = $this->databaza->prepare("SELECT * FROM articles");
        try {
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return null;
        }

    }

    function fetchArticlesById($id)
    {
        $stmt = $this->databaza->prepare("SELECT * FROM articles WHERE id=:id");
        try {
            $stmt->execute(array(":id"=>$id));
            return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return null;
        }

    }

    function fetchUserIdOfArticle($id): int
    {
        $stmt = $this->databaza->prepare("SELECT user_id FROM articles WHERE id=:id");
        try {
            $stmt->execute(array(":id"=>$id));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $i = 0;
            $pom = null;
            foreach($result as $value){
                $pom = $value;
                $i++;
            }
            return $pom;

        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return 0;
        }

    }




    function fetchCommentsByArticleId($id_article)
    {
        $stmt = $this->databaza->prepare("SELECT * FROM comments WHERE id_article=:id_article");
        try {
            $stmt->execute(array(":id_article"=>$id_article));
            return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return null;
        }
    }

    function fetchComments()
    {
        $stmt = $this->databaza->prepare("SELECT * FROM comments");
        try {
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return null;
        }

    }

    public function deleteRow($id)
    {
            $sql = "DELETE FROM articles WHERE id=:id";
            $stmt = $this->databaza->prepare($sql);
            try {
                $stmt->execute([':id' => $id]);
                if ($stmt->rowCount() > 0) {
                    $prem = true;
                }
                return $prem;
            } catch (PDOException $e) {
                echo "Error: " . $e->getMessage();
                return false;
            }
    }

    public function editArticle($heading, $text, $id, $edit_date)
    {
        $sql = 'UPDATE articles SET heading=:heading, text=:text, edit_date=:edit_date WHERE id=:id';
        try {
            $stmt = $this->databaza->prepare($sql);
            $stmt->execute(array(":heading"=>$heading, ":text"=>$text, ":edit_date"=>$edit_date, ":id"=>$id));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
         }
    }

    function addUser(User $user): bool
    {
        if ($this->checkUser($user)) {
            $stmt = $this->databaza->prepare('INSERT INTO users (username, password, role, firstname, 
                                                    surname, age, weight, height, 
                                                    handedness, playmode, color) VALUES(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->execute([$user->getUsername(), $user->getPassword(), $user->getRole(),
                            $user->getFirstname(), $user->getSurname(), $user->getAge(),
                            $user->getWeight(), $user->getHeight(), $user->getHandedness(),
                            $user->getPlaymode(), $user->getColor()]);
            return true;
        } else {
            return false;
        }
    }

    function checkUser(User $user): bool
    {
        $sql = 'SELECT id FROM users WHERE username=?';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([$user->getUsername()]);
        if ($result = $stmt->fetch()) {
            return false;
        } else {
            return true;
        }
    }


    function signUser($username, $password): bool
    {

        $sql = 'SELECT id FROM users WHERE username=:username AND password=:password';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute(array(":username"=>$username, ":password"=>$password));
        if ($result = $stmt->fetch()) {
            return true;
        } else {
            return false;
        }
    }

    function getAllUsers()
    {
        $stmt = $this->databaza->prepare("SELECT * FROM users");
        try {
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return null;
        }

    }

    function getUser($username, $password)
    {
        $sql = 'SELECT firstname, surname FROM users WHERE username=:username AND password=:password';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute(array(":username"=>$username, ":password"=>$password));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    function getUserIdFromUsers($username, $password): int
    {
        $sql = 'SELECT id FROM users WHERE username=:username AND password=:password';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute(array(":username"=>$username, ":password"=>$password));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $i = 0;
        $pom = null;
        foreach($result as $value){
            $pom = $value;
            $i++;
        }
        return $pom;
    }



}