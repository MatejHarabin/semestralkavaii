<?php

//namespace Databaza;

class Article
{
    private $id;
    private $heading;
    private $text;
    private $publish_date;
    private $edit_date;
    private $user_id;


    public function __construct($heading = null, $text = null, $publish_date = null, $edit_date = null, $user_id = null)
    {
        if ($heading!==null) $this->heading = $heading;
        if ($text!==null) $this->text = $text;
        if ($publish_date!==null) $this->publish_date = $publish_date;
        $this->edit_date = $edit_date;
        if ($user_id!==null) $this->user_id = $user_id;

    }

    /**
     * @return mixed
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * @param mixed $heading
     */
    public function setHeading($heading): void
    {
        $this->heading = $heading;
    }

     /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($pId)
    {
        $this->id = $pId;
    }



    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * @param mixed $publish_date
     */
    public function setPublishDate($publish_date): void
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return mixed|null
     */
    public function getEditDate()
    {
        return $this->edit_date;
    }

    /**
     * @param mixed|null $edit_date
     */
    public function setEditDate($edit_date): void
    {
        $this->edit_date = $edit_date;
    }



    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

}

