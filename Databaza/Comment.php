<?php

class Comment
{

    private $id_comment;
    private $id_article;
    private $publish_date;
    private $text;


    public function __construct($id_article = null, $publish_date = null, $text = null)
    {
        if ($id_article!==null) $this->id_article = $id_article;
        if ($publish_date!==null) $this->publish_date = $publish_date;
        if ($text!==null) $this->text = $text;

    }

    /**
     * @return mixed
     */
    public function getIdComment()
    {
        return $this->id_comment;
    }

    /**
     * @param mixed $id_comment
     */
    public function setIdComment($id_comment): void
    {
        $this->id_comment = $id_comment;
    }

    /**
     * @return mixed
     */
    public function getIdArticle()
    {
        return $this->id_article;
    }

    /**
     * @param mixed $id_article
     */
    public function setIdArticle($id_article): void
    {
        $this->id_article = $id_article;
    }

    /**
     * @return mixed
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * @param mixed $publish_date
     */
    public function setPublishDate($publish_date): void
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }



}