<?php

class User
{
    private $id;
    private $username;
    private $password;
    private $role;
    private $firstname;
    private $surname;
    private $age;
    private $weight;
    private $height;
    private $handedness;
    private $playmode;
    private $color;

    public function __construct($username = null, $password = null, $role = null, $firstname = null, $surname = null,
                                $age = null, $weight = null, $height = null, $handedness = null, $playmode = null, $color = null)
    {
        if($username !== null) $this->username = $username;
        if($password !== null) $this->password = $password;
        if($role !== null) $this->role = $role;
        if($firstname !== null) $this->firstname = $firstname;
        if($surname !== null) $this->surname = $surname;
        if($age !== null) $this->age = $age;
        if($weight !== null) $this->weight = $weight;
        if($height !== null) $this->height = $height;
        if($handedness !== null) $this->handedness = $handedness;
        if($playmode !== null) $this->playmode = $playmode;
        if($color !== null) $this->color = $color;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($pId)
    {
        $this->id = $pId;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getHandedness()
    {
        return $this->handedness;
    }

    /**
     * @param mixed $handedness
     */
    public function setHandedness($handedness): void
    {
        $this->handedness = $handedness;
    }

    /**
     * @return mixed
     */
    public function getPlaymode()
    {
        return $this->playmode;
    }

    /**
     * @param mixed $playmode
     */
    public function setPlaymode($playmode): void
    {
        $this->playmode = $playmode;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }



}
