<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matej Harabin - tréner stolného tenisu</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="Prihlasovanie.css" >
</head>
<body>

<?php if (isset($_GET['success'])) { ?>

    <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
        <?php echo $_GET['success']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php } ?>

<?php if (isset($_GET['error'])) { ?>

    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
        <?php echo $_GET['error']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php } ?>


<form id="register_form" action="../Logika/User/sign.php" method="post">
        <div class="wrapper">
            <div class="form-area">
                <div class="form-text">
                    <h1> Prihlásiť sa</h1>
                    <div class="Meno">
                        <i class="fa fa-users"></i>
                        <input type="text" placeholder="Použ.meno" name="username" required>
                    </div>
                    <div class="Heslo">
                        <i class="fa fa-unlock-alt"></i>
                        <input type="password" placeholder="Heslo" name="password" required>
                    </div>

                    <button name="sign">Prihlásiť</button>
                    <p>
                        Nový používateľ?
                    </p>

                    <a href="../Registracia/Registracia.php" class="btn btn-success" role="button">Zaregistruj sa!</a>
                    <br>

                    <a href="../Domovska/Domovska.php" id="Domov">Domov</a>
                </div>
            </div>
        </div>
</form>

</body>
</html>